//
//  ViewController.m
//  Calculator
//
//  Created by Admin on 11.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "ViewController.h"



@implementation ViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    self.model = [[CalculatorModel alloc] init];
}


- (IBAction)PressDigit:(UIButton *)sender {
    _lastButton = sender.currentTitle;
    if (self.isNumberInputting){
        if (![sender.currentTitle isEqualToString:@"."] || ![self.MyLabel.text containsString:@"."]) {
            self.MyLabel.text = [self.MyLabel.text stringByAppendingString:sender.currentTitle];
        }
    }
    
    else {
        if ([sender.currentTitle isEqualToString:@"."]){
            self.MyLabel.text = [NSString stringWithFormat:@"0."];
        }
        else {
            self.MyLabel.text = sender.currentTitle;
        }
        self.numberInputting = YES;
    }
}

- (IBAction)performOperation:(UIButton *)sender {
    self.numberInputting = NO;
   
    double value = self.MyLabel.text.doubleValue;
    NSString *operationString = sender.currentTitle;
    
    if (_lastButton != operationString){
        double result = [self.model performOperation:operationString withValue:value];
        _lastButton = operationString;
        self.MyLabel.text = [@(result) stringValue];
    }
}


@end
