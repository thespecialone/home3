//
//  SqrtOperation.m
//  Calculator
//
//  Created by Admin on 11.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "SqrtOperation.h"

@implementation SqrtOperation

- (double) perform: (double) value {
    return sqrt(value);
}

@end
