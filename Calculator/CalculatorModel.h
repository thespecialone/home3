//
//  CalculatorModel.h
//  Calculator
//
//  Created by Admin on 11.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnaryOperation.h"
#import "BinaryOperation.h"

@interface CalculatorModel : NSObject

@property (nonatomic, strong) NSDictionary *operations;
@property (nonatomic, strong) BinaryOperation *curentBinaryOperation;

- (double) performOperation: (NSString *) operationString withValue: (double) value;


@end
