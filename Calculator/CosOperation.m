//
//  CosOperation.m
//  Calculator
//
//  Created by Admin on 11.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "CosOperation.h"

@implementation CosOperation

- (double) perform: (double) value {
    return cos(value);
}
@end
