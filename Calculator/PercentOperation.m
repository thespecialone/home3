//
//  PercentOperation.m
//  Calculator
//
//  Created by Admin on 11.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "PercentOperation.h"

@implementation PercentOperation

- (double) perform: (double) value {
    return value / 100;
}

@end
