//
//  ViewController.h
//  Calculator
//
//  Created by Admin on 11.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculatorModel.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *MyLabel;
@property (nonatomic, assign, getter = isNumberInputting) BOOL numberInputting;
@property (nonatomic, strong) CalculatorModel *model;
@property (nonatomic, strong) NSString *lastButton;
@end
