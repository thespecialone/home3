//
//  SignOperation.m
//  Calculator
//
//  Created by Admin on 11.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "SignOperation.h"

@implementation SignOperation


- (double) perform: (double) value {
    return -value;
}

@end
