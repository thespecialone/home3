//
//  SinOperation.m
//  Calculator
//
//  Created by Admin on 11.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "SinOperation.h"

@implementation SinOperation

- (double) perform: (double) value {
    return sin(value);
}

@end
