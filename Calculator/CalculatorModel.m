//
//  CalculatorModel.m
//  Calculator
//
//  Created by Admin on 11.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "CalculatorModel.h"
#import "SumOperation.h"
#import "SignOperation.h"
#import "EqualOperation.h"
#import "SubOperation.h"
#import "CleanOperation.h"
#import "PercentOperation.h"
#import "MulOperation.h"
#import "DelOperation.h"
#import "DegreeOperation.h"
#import "SinOperation.h"
#import "CosOperation.h"
#import "TanOperation.h"
#import "SqrtOperation.h"

@implementation CalculatorModel

- (instancetype) init {
    self = [super init];
    if (self){
        _operations = @{
                        @"+": [[SumOperation alloc] init],
                        @"+-": [[SignOperation alloc] init],
                        @"=": [[EqualOperation alloc] init],
                        @"-": [[SubOperation alloc] init],
                        @"%": [[PercentOperation alloc] init],
                        @"*": [[MulOperation alloc] init],
                        @"/": [[DelOperation alloc] init],
                        @"AC": [[CleanOperation alloc] init],
                        @"cos": [[CosOperation alloc] init],
                        @"sin": [[SinOperation alloc] init],
                        @"tan": [[TanOperation alloc] init],
                        @"x^y": [[DegreeOperation alloc] init],
                        @"√": [[SqrtOperation alloc] init],
                        };
    }
    return self;
}

-(id) operationForOperationString: (NSString *) operationString {
    id operation = [self.operations objectForKey:operationString];
    if (!operation){
        operation = [[UnaryOperation alloc] init];     }
    return operation;
}

-(double) performOperation: (NSString*) operationString withValue:(double)value {
    
    id operation = [self operationForOperationString:operationString];
    
    if ([operation isKindOfClass:[EqualOperation class]]){
        if (self.curentBinaryOperation){
            value = [self.curentBinaryOperation performWithSecondArgument:value];         }
    }
    
    if ([operation isKindOfClass:[UnaryOperation class]]){
        return [operation perform: value];
    }
    
    if ([operation isKindOfClass:[BinaryOperation class]]){
        self.curentBinaryOperation = operation;
        self.curentBinaryOperation.firstArgument = value;
        return value;
    }
    return DBL_MAX;
    
}

@end
